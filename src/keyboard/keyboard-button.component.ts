import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'keyboard-button',
    template: `<button ion-button type='button' (click)='alert()'>{{content}}</button>`
})
export class KeyboardButtonComponent implements OnInit {

    @Input() content: String;

    constructor() { }

    ngOnInit() { }

    alert() {
        console.log(this.content);
    }
}