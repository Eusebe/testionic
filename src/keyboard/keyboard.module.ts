import { NgModule } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';

import { KeyboardComponent }   from './keyboard.component';
import { KeyboardButtonComponent }   from './keyboard-button.component';

@NgModule({
    imports: [],
    exports: [KeyboardComponent],
    entryComponents: [KeyboardComponent],
    declarations: [KeyboardComponent, KeyboardButtonComponent],
    providers: [],
})
export class KeyboardModule { }
