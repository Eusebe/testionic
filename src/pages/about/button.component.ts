import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'custom-button',
    template: `<button ion-button>a component inside AppModule</button>`
})
export class ButtonComponent implements OnInit {
    constructor() { }

    ngOnInit() { }
}